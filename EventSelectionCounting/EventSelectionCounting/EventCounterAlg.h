#ifndef EVENT_COUNTER_ALG_H
#define EVENT_COUNTER_ALG_H

#include "AthenaBaseComps/AthAlgorithm.h"
#include "SelectionHelpers/SysReadSelectionHandle.h"
#include "SystematicsHandles/SysReadHandle.h"
#include "xAODEventInfo/EventInfo.h"

#include <atomic>

// This algorithm counts the current number of events and writes/appends the result
// to a json file in the format of a list of (name, count) pairs; the name will be set
// to `countID'
class EventCounterAlg: public AthAlgorithm
{
private:
  unsigned long long m_count;

  // algorithm options
  Gaudi::Property<std::string> m_count_name {
    this, "countID", "", "Name of the event count"};

  Gaudi::Property<std::string> m_counts_output_path {
    this, "countsOutputPath", "", "Name of output json file"};

public:
  EventCounterAlg(const std::string& name, ISvcLocator* loc);

  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;
};

#endif
