#include "errorLogger.hh"

#include "xAODJet/Jet.h"

#include <filesystem>

ErrorLogger getLogger(H5::Group& group, const std::string filename) {
  using L = LoggerInputs;
  H5Utils::Consumers<const LoggerInputs&> cons;
  cons.add<float>("jet_pt", [](const L& l) { return l.jet->pt(); });
  cons.add<float>("jet_eta", [](const L& l) { return l.jet->eta(); });
  cons.add<float>("parent_pt", [](const L& l) { return l.parent->eta(); });
  cons.add<int>("jet_rank", [](const L& l) {return l.jet_rank;});
  cons.add<int>("missing_tracks", [](const L& l) {return l.missing_tracks;});
  std::filesystem::path filepath(filename);
  return H5Utils::makeWriter<0>(group, filepath.filename().string(), cons);
}
