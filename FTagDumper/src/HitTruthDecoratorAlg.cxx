#include "HitTruthDecoratorAlg.h"

#include "StoreGate/WriteDecorHandle.h"
#include "StoreGate/ReadDecorHandle.h"

#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "MCTruthClassifier/IMCTruthClassifier.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "AthLinks/ElementLink.h"
#include "xAODTracking/TrackParticle.h"

HitTruthDecoratorAlg::HitTruthDecoratorAlg(
    const std::string &name, ISvcLocator *loc) : AthReentrantAlgorithm(name, loc),
                                                 m_Barcode("sihit_barcode"), 
                                                 m_Energy("sihit_energyDeposit")
{
}

StatusCode HitTruthDecoratorAlg::initialize()
{
  ATH_MSG_DEBUG("Inizializing " << name() << "... ");
  ATH_CHECK(m_Truths.initialize());
  ATH_CHECK(m_JetPixelCluster.initialize());
  ATH_CHECK(m_JetSCTCluster.initialize());

  ATH_CHECK(m_JetPixelClusterEnergyDep_B.initialize());
  ATH_CHECK(m_JetPixelClusterEnergyDep_C.initialize());
  ATH_CHECK(m_JetPixelClusterTruthBarcode.initialize());

  ATH_CHECK(m_JetSCTClusterEnergyDep_B.initialize());
  ATH_CHECK(m_JetSCTClusterEnergyDep_C.initialize());
  ATH_CHECK(m_JetSCTClusterTruthBarcode.initialize());

  return StatusCode::SUCCESS;
}

StatusCode HitTruthDecoratorAlg::execute(const EventContext &ctx) const
{
  ATH_MSG_DEBUG("Executing " << name() << "... ");
  SG::ReadHandle<xAOD::TruthParticleContainer> Truths{m_Truths, ctx};

  SG::ReadHandle<xAOD::TrackMeasurementValidationContainer> JetPixelCluster{m_JetPixelCluster, ctx};
  SG::ReadHandle<xAOD::TrackMeasurementValidationContainer> JetSCTCluster{m_JetSCTCluster, ctx};

  SG::WriteDecorHandle<xAOD::TrackMeasurementValidationContainer, float> JetPixelClusterEnergyDep_B{m_JetPixelClusterEnergyDep_B, ctx};
  SG::WriteDecorHandle<xAOD::TrackMeasurementValidationContainer, float> JetPixelClusterEnergyDep_C{m_JetPixelClusterEnergyDep_C, ctx};
  SG::WriteDecorHandle<xAOD::TrackMeasurementValidationContainer, int> JetPixelClusterTruthHad_barcode{m_JetPixelClusterTruthBarcode, ctx};

  SG::WriteDecorHandle<xAOD::TrackMeasurementValidationContainer, float> JetSCTClusterEnergyDep_B{m_JetSCTClusterEnergyDep_B, ctx};
  SG::WriteDecorHandle<xAOD::TrackMeasurementValidationContainer, float> JetSCTClusterEnergyDep_C{m_JetSCTClusterEnergyDep_C, ctx};
  SG::WriteDecorHandle<xAOD::TrackMeasurementValidationContainer, int> JetSCTClusterTruthHad_barcode{m_JetSCTClusterTruthBarcode, ctx};

  if (!JetPixelCluster.isValid())
  {
    ATH_MSG_ERROR("Couldn't find Pixel");
    return StatusCode::FAILURE;
  }
  if (!JetSCTCluster.isValid())
  {
    ATH_MSG_ERROR("Couldn't find SCT");
    return StatusCode::FAILURE;
  }
  if (!Truths.isValid())
  {
    ATH_MSG_ERROR("Couldn't find tracks");
    return StatusCode::FAILURE;
  }


  std::vector<int> bhadron_barcodes, chadron_barcodes;

  for (auto part : *Truths) {
    if (part->isBottomHadron()) {
       bhadron_barcodes.push_back(part->barcode());
    }
    else if (part->isCharmHadron()) {
       chadron_barcodes.push_back(part->barcode());
    }
  }  




  for (auto hit : *JetPixelCluster)
  {
    float sumE = 0;
    for(float E:m_Energy(*hit))
    {
      sumE +=E;
    }
    float sumEDepositsFromB = 0;
    float sumEDepositsFromC = 0;
    if (m_Barcode(*hit).size() != m_Energy(*hit).size()) 
    {
      ATH_MSG_ERROR("Vector of Barcodes and Vector of Energies don't have the same length");
    }
    int TruthBcode = -1;
    for (long unsigned int pos = 0; pos < m_Barcode(*hit).size(); pos++)
    {
      int barcode=(m_Barcode(*hit))[pos];
      float energy = (m_Energy(*hit))[pos];
      if (std::find(bhadron_barcodes.begin(), bhadron_barcodes.end(), barcode) != bhadron_barcodes.end()) 
      {
        sumEDepositsFromB +=energy/sumE;
      }
      if (std::find(chadron_barcodes.begin(), chadron_barcodes.end(), barcode) != chadron_barcodes.end()) 
      {
        sumEDepositsFromC +=energy/sumE;
      }
    }
    if ((m_Barcode(*hit)).size() > 0)
    {
      TruthBcode = (m_Barcode(*hit))[0];
    }
    JetPixelClusterEnergyDep_B(*hit) = sumEDepositsFromB;
    JetPixelClusterEnergyDep_C(*hit) = sumEDepositsFromC;
    JetPixelClusterTruthHad_barcode(*hit) = TruthBcode;

  }
  for (auto hit : *JetSCTCluster)
  {
    JetSCTClusterEnergyDep_B(*hit) = -1;
    JetSCTClusterEnergyDep_C(*hit) = -1;
    JetSCTClusterTruthHad_barcode(*hit) = -1;

  }


  return StatusCode::SUCCESS;
}
