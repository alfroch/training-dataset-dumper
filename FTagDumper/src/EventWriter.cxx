#include "EventWriter.hh"
#include "EventOutputs.hh"
#include "EventWriterConfig.hh"
#include "HDF5Utils/Writer.h"

#include "add_event_info.hh"

EventWriter::EventWriter(
  H5::Group& g,
  const EventWriterConfig& c):
  m_writer(nullptr)
{
  using H5Utils::Compression;

  Compression f = Compression::STANDARD;
  Compression h = c.force_full_precision ?
    Compression::STANDARD : Compression::HALF_PRECISION;

  Writer::element_type::consumer_type vars;

  // keep track of the variables we don't use for custom variables
  std::set<std::string> unused(c.vars.customs.begin(), c.vars.customs.end());

  // go through and check to see if anyone has asked for a few custom
  // variable definitions
  auto pop = [&unused, &vars](const std::string& name, auto func, auto def) {
    if (auto nh = unused.extract(name)) {
      vars.add(name, func, def);
    }
  };
  pop(
    "firstJetIndex",
    [](const EventOutputs& o) { return o.first_jet_index; },
    0);
  pop(
    "nJets",
    [](const EventOutputs& o) { return o.next_jet_index - o.first_jet_index; },
    0);

  // send the unused stuff to the functions below. I don't just build
  // the vector from the map because what I have below preserves the
  // order of the inputs. Which is important. Because.
  std::vector<std::string> unused_vec;
  for (const auto& key: c.vars.customs) {
    if (unused.contains(key)) unused_vec.push_back(key);
  }
  add_event_info(vars, unused_vec, f);
  add_event_info(vars, c.vars.compressed, h);
  m_writer = std::make_unique<Writer::element_type>(g, c.name, vars);
}

EventWriter::~EventWriter() {
  m_writer->flush();
}

void EventWriter::write(const EventOutputs& o) {
  m_writer->fill(o);
}
