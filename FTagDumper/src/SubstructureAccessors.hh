#ifndef SUBSTRUCTURE_ACCESSORS_HH
#define SUBSTRUCTURE_ACCESSORS_HH

namespace xAOD {
  class Jet_v1;
  typedef Jet_v1 Jet;
}


#include "AthContainers/AuxElement.h"

struct SubstructureAccessors {
  typedef SG::AuxElement AE;
  SubstructureAccessors();
  float c2(const xAOD::Jet&);
  float d2(const xAOD::Jet&);
  float e3(const xAOD::Jet&);
  float tau21(const xAOD::Jet&);
  float tau32(const xAOD::Jet&);
  float fw20(const xAOD::Jet&);

  AE::ConstAccessor<float> ecf1;
  AE::ConstAccessor<float> ecf2;
  AE::ConstAccessor<float> ecf3;

  AE::ConstAccessor<float> tau1wta;
  AE::ConstAccessor<float> tau2wta;
  AE::ConstAccessor<float> tau3wta;

  AE::ConstAccessor<float> fw2;
  AE::ConstAccessor<float> fw0;
};


#endif
