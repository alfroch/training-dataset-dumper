/*
  Copyright (C) 2002-2045 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TRUTH_JET_PV_DECORATOR_H
#define TRUTH_JET_PV_DECORATOR_H

#include "xAODBase/IParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "AthenaBaseComps/AthReentrantAlgorithm.h"

#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthVertexContainer.h"

class TruthJetPrimaryVertexDecoratorAlg: public AthReentrantAlgorithm
{
public:
  TruthJetPrimaryVertexDecoratorAlg(
    const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute (const EventContext&) const override;
private:
  using JC = xAOD::IParticleContainer;
  using IPLV = std::vector<ElementLink<xAOD::IParticleContainer>>;
  SG::WriteDecorHandleKey<JC> m_decPv {
    this, "jetPvDecorator", "", "decorator for jets"};
  SG::ReadHandleKey<xAOD::TruthEventContainer> m_events {
    this, "truthEvents", "TruthEvents", "events with the PV"};
  Gaudi::Property<bool> m_useSignalProcessVertex {
    this, "useSignalProcessVertex", true, "Switch between using signalProcessVertex (works on PHYSVAL) and TruthPrimaryVertices container (AOD, FTAG1) to access truth jet origin"};
};

#endif
