#ifndef EVENT_WRITER_HH
#define EVENT_WRITER_HH


namespace H5 {
  class Group;
}
namespace xAOD {
  class EventInfo_v1;
  typedef EventInfo_v1 EventInfo;
}

struct EventWriterConfig;
struct EventOutputs;

#include <memory>

namespace H5Utils {
  template<size_t N, typename T> class Writer;
}



class EventWriter
{
public:
  using Writer = std::unique_ptr<H5Utils::Writer<0, const EventOutputs&>>;
  EventWriter(
    H5::Group& output_file,
    const EventWriterConfig& jet);
  ~EventWriter();
  EventWriter(EventWriter&&) = delete;
  EventWriter(EventWriter&) = delete;
  EventWriter operator=(EventWriter&) = delete;
  void write(const EventOutputs&);
private:
  Writer m_writer;
};

#endif // EVENT_WRITER_HH
