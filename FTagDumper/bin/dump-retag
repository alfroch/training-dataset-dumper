#!/usr/bin/env python

"""

Setup using setup/athena.sh is required for this script.

This runs "retagging" i.e. it overwrites the current b-tagging on jets
with new information. It has to run in Athena, to access track
extrapolators and secondary vertex reconstruction.

You are able to able to run the retagging over combined the LRT and
standard tracks with the -M option.

You ca provide a list of jet systematis by the -j option

"""

from argparse import ArgumentParser
import sys

from FTagDumper.dumper import getMainConfig as getConfig

from FTagDumper import dumper
from FTagDumper import jetUtil as jetUtil

import warnings
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
try:
    from FTagDumper.retag import retagging
except ModuleNotFoundError:
    def retagging(*args, **kwargs):
        warnings.warn(
            "retagging can't be set up, running no-op retagging mode",
            category=NoopWarning,
            stacklevel=2,
        )
        return ComponentAccumulator()

class NoopWarning(ImportWarning):
    pass

def get_args():
    """
    Extend the base dumper argument parser.
    """
    parser = ArgumentParser(
        formatter_class=dumper.DumperHelpFormatter,
        parents=[dumper.base_parser(__doc__, add_help=False)],
    )
    track_opts = parser.add_mutually_exclusive_group()

    track_opts.add_argument(
        "-M",
        "--merge-tracks",
        action="store_true",
        help="merge the standard and large d0 track collections before everything else.\n"
             "only works in releases after 22.0.59 (2022-03-08T2101)",
    )
    parser.add_argument(
        '--allow-noop',
        action='store_true'
    )
    return parser.parse_args()

# copied this from JetTagConfig/BTaggingConfigFlags
def getNNs(flags):
     caldir = 'BTagging/20231205/GN2v01/antikt4empflow'
     pf_nns = [f'{caldir}/network_fold{n}.onnx' for n in range(4)]
     return {
         OUTPUT_JETS: [
             {
                 'folds': pf_nns,
             },
         ]
     }

OUTPUT_JETS = 'outputJets'

def run():

    args = get_args()
    if not args.allow_noop:
        warnings.simplefilter('error', NoopWarning)

    flags = dumper.update_flags(args)
    flags.BTagging.RunFlipTaggers = True
    flags.BTagging.NNs = getNNs(flags)
    flags.lock()

    config = dumper.combinedConfig(args.config_file)
    jet_sys = config['jet_systematics']
    track_sys = config['track_systematics']
    jet_collection = config['dumper']['jet_collection']
    jet_sigma = config['jet_sigma']

    ca = getConfig(flags)
    ca.merge(dumper.getBlocksConfig(flags, args))

    ca.merge(
        jetUtil.applyJetSys(
            jet_sys,
            jet_collection,
            jet_sigma,
            output_jet_collection=OUTPUT_JETS,
        )
    )

    # add full blown retagging
    ca.merge(
        retagging(
            flags = flags,
            merge = args.merge_tracks,
            track_collection = config['track_container'],
            jet_collection = OUTPUT_JETS,
            track_systs = track_sys,
            decorate_track_truth = config['decorate_track_truth'],
            original_jet_collection=jet_collection,
        )
    )

    # slight munging for the config file: use the varied jets from above
    config['dumper']['jet_collection'] = OUTPUT_JETS
    # add dumper
    ca.merge(dumper.getDumperConfig(args, config))

    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
