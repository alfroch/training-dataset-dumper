from dataclasses import dataclass
from pathlib import Path

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from .BaseBlock import BaseBlock


@dataclass
class MultifoldTagger(BaseBlock):
    """Run a multifold tagger.

    Parameters
    ----------
    nn_paths : list[str]
        List of paths to the neural network files.
    alg_name : str
        Name of the algorithm. If None, the name will be
        extracted from the nn_paths. By default None
    target : str
        Whether to tag the BTagging object or the Jet.
    jet_collection : str | None
        Name of the jet collection to decorate. If None, uses the jet collection
        from the dumper configuration.
    remap : dict
        Remap input and output variable names.
    fold_hash_name : str
        Name of the fold hash variable.
    constituents : str
        Name of the constituent container.
    default_zero_tracks : bool
        Bool to decide how to work with zero tracks. By default False.
    """
    nn_paths: list[str]
    alg_name: str = None
    target: str = "BTagging"
    jet_collection: str = None
    remap: dict = None
    fold_hash_name: str = "jetFoldHash"
    constituents: str = "InDetTrackParticles"
    default_zero_tracks: bool = False

    def __post_init__(self):

        if self.jet_collection is None:
            self.jet_collection = self.dumper_config["jet_collection"]
        if self.remap is None:
            self.remap = {}

        if self.target == "BTagging":
            self.deco_alg = CompFactory.FlavorTagDiscriminants.BTagDecoratorAlg
            self.container = 'BTagging_{}'.format(
                self.jet_collection.replace("Jets", ""))
            self.track_link_type = "TRACK_PARTICLE"
        elif self.target == "Jet":
            self.deco_alg = CompFactory.FlavorTagDiscriminants.JetTagDecoratorAlg
            self.container = self.jet_collection
            self.track_link_type = "IPARTICLE"
        else:
            raise ValueError(f"Unknown target {self.target}")
        
        if self.alg_name is None:
            self.alg_name = '_'.join(Path(self.nn_paths[0]).parts[2:-1])

        if "BTagTrackToJetAssociator" in self.remap:
            self.alg_name = f'{self.remap["BTagTrackToJetAssociator"]}_{self.alg_name}' 
  
    def to_ca(self):
        ca = ComponentAccumulator()
        ca.addEventAlgo(
            self.deco_alg(
                name=f'{self.alg_name}_Alg',
                container=self.container,
                constituentContainer=self.constituents,
                decorator=CompFactory.FlavorTagDiscriminants.MultifoldGNNTool(
                    name=f'{self.alg_name}_Tool',
                    foldHashName=self.fold_hash_name,
                    nnFiles=self.nn_paths,
                    variableRemapping=self.remap,
                    trackLinkType=self.track_link_type,
                    defaultZeroTracks=self.default_zero_tracks,
                )
            )
        )
        return ca
