from dataclasses import dataclass

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from JetAnalysisAlgorithms.BJetCalibCAConfig import BJetCalibrationAlgCfg

from .BaseBlock import BaseBlock

@dataclass
class MuonCorrDecorator(BaseBlock):
    """Do Muon Correction with BJetCalibrationTool .

    """

    def to_ca(self):
        ca = ComponentAccumulator()
        ca.addService(CompFactory.CP.SystematicsSvc("SystematicsSvc"))
        ca.merge(BJetCalibrationAlgCfg(
            self.flags,
            jets = 'AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets',
            jetsOut = 'AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets_muonCorr',
            muons = 'Muons',
            doPtCorr = False,
        ))

        return ca
